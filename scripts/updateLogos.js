const fetch = require("node-fetch");
const DOMParser = require("dom-parser");
const fs = require('fs');
const https = require('https');



fetchLogos()

function saveImageToDisk(url, localPath) {
  let fullUrl = url;
  let file = fs.createWriteStream(localPath);
  let request = https.get(url, function (response) {
    response.pipe(file);
  });
}

function fetchLogos() {
  fetch('https://statsapi.web.nhl.com/api/v1/teams/')
    .then(function (response) {
      if (response.status !== 200) {
        console.log('Unable to update team logos. NHL Status Code: ' +
          response.status);
        return;
      }

      response.json().then(function (data) {
        data.teams.forEach(element => {
          if (element.officialSiteUrl != 'http://www.tampabaylightning.com/') {
            scrapeLogoURL(element.officialSiteUrl, element.id)
          }
        });
      });
    })
    .catch(function (err) {
      console.log('Fetch Error', err);
    });
}

function scrapeLogoURL(officialSiteUrl, name) {
  fetch(officialSiteUrl)
    .then(function (response) {
      if (response.status !== 200) {
        console.log('Unable to update team logos. NHL Status Code: ' +
          response.status);
        return;
      }

      response.text().then(function (data) {
        let parser = new DOMParser();
        let doc = parser.parseFromString(data, 'text/xml');
        let url = doc.getElementsByClassName('megamenu-club-logobar__logo')[0].childNodes[1].attributes.find(obj => {
          return obj.name === 'src'
        }).value;
        if (url.substring(0, 4) !== 'http') {
          url = 'https:' + url
        }
        console.log(url)
        saveImageToDisk(url, './public/images/teamLogos/' + name + url.substring(url.length - 4, url.length));
      });
    })
    .catch(function (err) {
      console.log('ScrapeLogo Fetch Error', err);
      return;
    });
}